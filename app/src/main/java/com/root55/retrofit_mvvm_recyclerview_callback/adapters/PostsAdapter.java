package com.root55.retrofit_mvvm_recyclerview_callback.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.root55.retrofit_mvvm_recyclerview_callback.R;
import com.root55.retrofit_mvvm_recyclerview_callback.pojo.CategoryResponse;

import java.util.ArrayList;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> {
    ArrayList<CategoryResponse> Posts;

   public PostsAdapter(ArrayList<CategoryResponse> Posts) {
        this.Posts = Posts;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
        PostViewHolder viewHolder = new PostViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        holder.user_id.setText(Posts.get(position).getUserId());
        holder.post_id.setText(Posts.get(position).getId());
        holder.post_body.setText(Posts.get(position).getBody());


    }

    @Override
    public int getItemCount() {
        return Posts.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        TextView user_id, post_id, post_body;

        private PostViewHolder(@NonNull View itemView) {
            super(itemView);
            user_id = itemView.findViewById(R.id.user_id);
            post_id = itemView.findViewById(R.id.post_id);
            post_body = itemView.findViewById(R.id.post_body);
        }
    }
}
