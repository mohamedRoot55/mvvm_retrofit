package com.root55.retrofit_mvvm_recyclerview_callback.ui.main;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.root55.retrofit_mvvm_recyclerview_callback.adapters.PostsAdapter;
import com.root55.retrofit_mvvm_recyclerview_callback.pojo.CategoryResponse;
import com.root55.retrofit_mvvm_recyclerview_callback.wepservice.ApiService;
import com.root55.retrofit_mvvm_recyclerview_callback.wepservice.WepServiceClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostViewModel extends ViewModel {
    MutableLiveData<ArrayList<CategoryResponse>> Posts  = new MutableLiveData<>();
    public   void getDataFromDB(){
        CallPostsApi() ;
    }

    private void CallPostsApi() {
        ApiService apiService = WepServiceClient.getRetrofit().create(ApiService.class) ;
       Call<List<CategoryResponse>>  call = apiService.getPosts() ;
       call.enqueue(new Callback<List<CategoryResponse>>() {
           @Override
           public void onResponse(Call<List<CategoryResponse>> call, Response<List<CategoryResponse>> response) {
               Posts.setValue((ArrayList<CategoryResponse>) response.body());
           }

           @Override
           public void onFailure(Call<List<CategoryResponse>> call, Throwable t) {

           }
       });
    }
}
