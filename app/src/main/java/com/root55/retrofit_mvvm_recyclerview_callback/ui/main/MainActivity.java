package com.root55.retrofit_mvvm_recyclerview_callback.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.root55.retrofit_mvvm_recyclerview_callback.R;
import com.root55.retrofit_mvvm_recyclerview_callback.adapters.PostsAdapter;
import com.root55.retrofit_mvvm_recyclerview_callback.pojo.CategoryResponse;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    RecyclerView mRecycler;
    PostsAdapter adapter;
    RecyclerView.LayoutManager manager;
    PostViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setUpViewModelProvider();
    }

    private void initViews() {
        mRecycler = findViewById(R.id.recycler);
    }

    private void setUpViewModelProvider() {
        viewModel = ViewModelProviders.of(this).get(PostViewModel.class);
        viewModel.getDataFromDB();



        viewModel.Posts.observe(this, new Observer<ArrayList<CategoryResponse>>() {
            @Override
            public void onChanged(ArrayList<CategoryResponse> categoryResponses) {
                adapter = new PostsAdapter(categoryResponses) ;
                manager = new LinearLayoutManager(MainActivity.this , RecyclerView.VERTICAL , false);
                mRecycler.setLayoutManager(manager);
                mRecycler.setAdapter(adapter);
            }
        });
    }

}


