package com.root55.retrofit_mvvm_recyclerview_callback.wepservice;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WepServiceClient {
    private static final String base_url = "https://jsonplaceholder.typicode.com/";
    private static Retrofit retrofit = null ;
    public static Retrofit getRetrofit(){
        if(retrofit == null ){
            retrofit = new Retrofit.Builder().baseUrl(base_url).addConverterFactory(GsonConverterFactory.create()).build() ;
        }
        return retrofit ;
    }

}
