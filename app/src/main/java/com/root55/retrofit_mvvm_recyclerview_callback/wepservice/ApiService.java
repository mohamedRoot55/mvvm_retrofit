package com.root55.retrofit_mvvm_recyclerview_callback.wepservice;

import com.root55.retrofit_mvvm_recyclerview_callback.pojo.CategoryResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {


    @GET("posts")
    Call<List<CategoryResponse>> getPosts() ;
}
